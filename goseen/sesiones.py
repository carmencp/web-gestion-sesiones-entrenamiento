from flask import (
    Blueprint, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from goseen.auth import login_required
from goseen.db import get_db

from urllib.parse import urlparse
from urllib.parse import parse_qs

# blueprint para las funciones relacionadas con las sesiones

# el url_prefix se añadirá a todas las URLs asociadas al blueprint
bp = Blueprint('sesiones', __name__, url_prefix='/sesiones')


''' código para la creación de una sesión,
@bp.route asocia la URL /crearSesion con la función de la vista para crear una sesión '''
@bp.route('/crearSesion', methods=('GET', 'POST'))
@login_required
def crearSesion():

    db = get_db()
    error = ""
    titulo=""
    descripcion=""
    array=[]

    if request.method == 'POST':
        titulo = request.form['titulo']
        descripcion = request.form['descripcion']
        array = request.form['array']

        if not titulo:
            error = 'Debes introducir un título para la sesión.'
        if error is "":
            try:
                db.execute(
                    'INSERT INTO sesion (array, titulo, descripcion, creador_id)'
                    ' VALUES (?, ?, ?, ?)',
                    (array, titulo, descripcion, g.usuario['id'])
                )
                db.commit()
            except db.IntegrityError:
                error = f"Ya existe una sesión con ese título."
            else:
                return redirect(url_for('sesiones.mostrarSesiones'))

    ''' Si el método es GET, el usuario se dispone a crear una sesión, por lo que,
    primero, se obtienen las categorías creadas por el usuario '''
    categorias = db.execute(
        'SELECT * FROM categoria WHERE creador_id = ? ORDER BY nombre',str(g.usuario['id'])
    ).fetchall()

    # después, se obtienen todos los vídeos subidos por el usuario
    videos = db.execute(
        'SELECT * FROM video WHERE creador_id = ? ORDER BY categoria_id',str(g.usuario['id'])
    ).fetchall()

    # busca qué categorías sí tienen vídeos y guarda los ids de esas categorías sin repetir
    idsCategorias = []
    for c in categorias:
        for v in videos:
            if c["id"] not in idsCategorias:
                if v["categoria_id"] == c["id"]:
                    idsCategorias.append(c["id"])

    # y obtiene los datos de la bd de cada una de esas categorías
    categoriasValidas = []
    for i in idsCategorias:
        cate = db.execute(
            'SELECT * FROM categoria WHERE id = ? AND creador_id = ? ORDER BY nombre',(i,str(g.usuario['id']))
        ).fetchone()
        categoriasValidas.append(cate)

    # por otro lado, guarda los ids de YouTube de cada vídeo para poder crear sesiones aleatorias
    ids = []
    for n in videos:
        ids.append(n["enlace"])
    # lo formateamos a string
    str1 = ','.join(ids)

    return render_template('sesiones/crearSesion.html', videos=videos, ids=str1, error=error, categoriasValidas=categoriasValidas, 
    titulo=titulo, descripcion=descripcion, array=array)


''' código para mostrar las sesiones creadas,
@bp.route asocia la URL /mostrarSesiones con la función de la vista para mostrar las sesiones '''
@bp.route('/mostrarSesiones')
@login_required
def mostrarSesiones():
    db = get_db()

    sesiones = db.execute(
        'SELECT * FROM sesion WHERE creador_id = ?',(str(g.usuario['id']))
    ).fetchall()

    return render_template('sesiones/mostrarSesiones.html', sesiones=sesiones)


''' código para visualizar una sesión,
@bp.route asocia la URL /verSesion con la función de la vista para visualizar una sesión '''
@bp.route('/verSesion')
@login_required
def verSesion():

    db = get_db()

    # recogemos el id de la sesión que hemos pasado por la URL
    requestUrl = request.url
    parsed_url = urlparse(requestUrl)
    captured_value = parse_qs(parsed_url.query)['id'][0]
    
    sesion = db.execute(
        'SELECT * FROM sesion WHERE id = ? AND creador_id = ?',
        (captured_value, str(g.usuario['id']))
    ).fetchone()

    return render_template('sesiones/verSesion.html', sesion=sesion)


# código para obtener una sesión a partir de su id
def get_sesion(id):
    sesion = get_db().execute(
        'SELECT * FROM sesion WHERE creador_id = ? AND id = ?',
        (str(g.usuario['id']), id)
    ).fetchone()

    if sesion is None:
        abort(404, f"No existe la sesión {id}.")

    if sesion['creador_id'] != g.usuario['id']:
        abort(403)

    return sesion


''' código para editar una sesión,
@bp.route asocia la URL /<int:id>/editarSesion con la función de la vista para editar una sesión.
Esta función toma un argumento, id. Este corresponde al <int:id> de la ruta y al id de la sesión. 
Flask capturará el id, se asegurará de que es un int, y lo pasará como el argumento id '''
@bp.route('/<int:id>/editarSesion', methods=('GET', 'POST'))
@login_required
def editarSesion(id):

    sesion = get_sesion(id)
    db = get_db()
    error = None

    if request.method == 'POST':
        titulo = request.form['titulo']
        descripcion = request.form['descripcion']
        array = request.form['array']

        if not titulo:
            error = 'Debes introducir un título para la sesión.'
        elif not array:
            error = 'Se debe elegir algún vídeo.'

        if error is None:
            try:
                db.execute(
                    'UPDATE sesion SET titulo = ?, descripcion = ?, array = ?'
                    ' WHERE id = ? AND creador_id = ?',
                    (titulo, descripcion, array, id, str(g.usuario['id']))
                )
                db.commit()
            except db.IntegrityError:
                error = f"Ya existe una sesión con ese título."
            else:
                return redirect(url_for('sesiones.mostrarSesiones'))

    ''' Si el método es GET, el usuario se dispone a editar una sesión, por lo que,
    primero, se obtienen las categorías creadas por el usuario '''
    categorias = db.execute(
        'SELECT * FROM categoria WHERE creador_id = ? ORDER BY nombre',str(g.usuario['id'])
    ).fetchall()

    # después, se obtienen todos los vídeos subidos por el usuario
    videos = db.execute(
        'SELECT * FROM video WHERE creador_id = ? ORDER BY categoria_id',str(g.usuario['id'])
    ).fetchall()

    # busca qué categorías sí tienen vídeos y guarda los ids de esas categorías sin repetir
    idsCategorias = []
    for c in categorias:
        for v in videos:
            if c["id"] not in idsCategorias:
                if v["categoria_id"] == c["id"]:
                    idsCategorias.append(c["id"])

    # y obtiene los datos de la bd de cada una de esas categorías
    categoriasValidas = []
    for i in idsCategorias:
        cate = db.execute(
            'SELECT * FROM categoria WHERE id = ? AND creador_id = ? ORDER BY nombre',(i,str(g.usuario['id']))
        ).fetchone()
        categoriasValidas.append(cate)

    return render_template('sesiones/editarSesion.html', sesion=sesion, videos=videos, error=error, 
    categoriasValidas=categoriasValidas)


''' código para eliminar una sesión,
Esta función toma un argumento, id. Este corresponde al <int:id> de la ruta y al id de la categoría. 
Flask capturará el id, se asegurará de que es un int, y lo pasará como el argumento id '''
@bp.route('/<int:id>/eliminarSesion', methods=('POST',))
@login_required
def eliminarSesion(id):

    db = get_db()
    db.execute('DELETE FROM sesion WHERE id = ? AND creador_id = ?',(id, str(g.usuario['id'])))
    db.commit()
    return redirect(url_for('sesiones.mostrarSesiones'))