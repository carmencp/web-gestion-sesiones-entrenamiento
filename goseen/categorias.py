from flask import (
    Blueprint, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from goseen.auth import login_required
from goseen.db import get_db

# blueprint para las funciones relacionadas con las categorías
# actuará de página principal
bp = Blueprint('categorias', __name__)

# código para la página principal, el index, donde se mostrarán las categorías creadas
@bp.route('/')
@login_required
def index():
    db = get_db()
    categorias = db.execute(
        'SELECT * FROM categoria WHERE creador_id = ?'
        ' ORDER BY nombre',(str(g.usuario['id']))
    ).fetchall()
    return render_template('categorias/index.html', categorias=categorias)


''' código para la creación de una categoría,
@bp.route asocia la URL /crearCategoria con la función de la vista para crear una categoría '''
@bp.route('/crearCategoria', methods=('GET', 'POST'))
@login_required
def crearCategoria():

    error = None

    if request.method == 'POST':
        nombre = request.form['nombre']
        descripcion = request.form['descripcion']

        if not nombre:
            error = 'Debes introducir un nombre para la categoría.'
        elif not descripcion:
            error = 'Debes introducir una descripción.'

        if error is None:
            try:
                db = get_db()
                db.execute(
                    'INSERT INTO categoria (nombre, descripcion, creador_id)'
                    ' VALUES (?, ?, ?)',
                    (nombre, descripcion, g.usuario['id'])
                )
                db.commit()
            except db.IntegrityError:
                error = f"Ya existe una categoría con ese nombre."
            else:
                return redirect(url_for('categorias.index'))

    return render_template('categorias/crearCategoria.html', error=error)


# código para obtener una categoría a partir de su id
def get_categoria(id):
    categoria = get_db().execute(
        'SELECT * FROM categoria WHERE id = ? AND creador_id = ?',
        (id, str(g.usuario['id']))
    ).fetchone()

    if categoria is None:
        abort(404, f"No existe la categoría {id}.")

    if categoria['creador_id'] != g.usuario['id']:
        abort(403)

    return categoria


''' código para editar una categoría,
@bp.route asocia la URL /<int:id>/editarCategoria con la función de la vista para editar una categoría.
Esta función toma un argumento, id. Este corresponde al <int:id> de la ruta y al id de la categoría. 
Flask capturará el id, se asegurará de que es un int, y lo pasará como el argumento id '''
@bp.route('/<int:id>/editarCategoria', methods=('GET', 'POST'))
@login_required
def editarCategoria(id):

    categoria = get_categoria(id)
    error = None

    if request.method == 'POST':
        nombre = request.form['nombre']
        descripcion = request.form['descripcion']

        if not nombre:
            error = 'Debes introducir un nombre para la categoría.'
        elif not descripcion:
            error = 'Debes introducir una descripción.'

        if error is None:
            try:
                db = get_db()
                db.execute(
                    'UPDATE categoria SET nombre = ?, descripcion = ?'
                    ' WHERE id = ? AND creador_id = ?',
                    (nombre, descripcion, id, str(g.usuario['id']))
                )
                db.commit()
            except db.IntegrityError:
                error = f"Ya existe una categoría con ese nombre."
            else:
                return redirect(url_for('categorias.index'))

    return render_template('categorias/editarCategoria.html', categoria=categoria, error=error)


''' código para eliminar una categoría y, por consiguiente, sus vídeos,
Esta función toma un argumento, id. Este corresponde al <int:id> de la ruta y al id de la categoría. 
Flask capturará el id, se asegurará de que es un int, y lo pasará como el argumento id '''
@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def eliminarCategoria(id):
    get_categoria(id)
    db = get_db()

    # obtenemos los vídeos de esa categoría
    videos = db.execute(
        'SELECT * FROM video WHERE categoria_id = ? AND creador_id = ?'
        ,(id, str(g.usuario['id']))
    ).fetchall()

    # si la categoría contiene vídeos, los borrará de la bd
    if videos:
        db.execute('DELETE FROM video WHERE id_categoria = ? AND creador_id = ?',(id, str(g.usuario['id'])))

    # elimina la categoría
    db.execute('DELETE FROM categoria WHERE id = ? AND creador_id = ?',(id, str(g.usuario['id'])))
    db.commit()
    return redirect(url_for('categorias.index'))