DROP TABLE IF EXISTS usuario;
DROP TABLE IF EXISTS categoria;
DROP TABLE IF EXISTS video;
DROP TABLE IF EXISTS sesion;

CREATE TABLE usuario (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre TEXT NOT NULL,
  apellidos TEXT,
  email TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE categoria (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre TEXT NOT NULL,
  descripcion TEXT NOT NULL,
  creador_id INTEGER NOT NULL,
  FOREIGN KEY (creador_id) REFERENCES usuario (id),
  UNIQUE(nombre,creador_id)
);

CREATE TABLE video (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  enlace TEXT NOT NULL,
  categoria_id INTEGER NOT NULL,
  creador_id INTEGER NOT NULL,
  FOREIGN KEY (categoria_id) REFERENCES categoria (id),
  FOREIGN KEY (creador_id) REFERENCES usuario (id),
  UNIQUE(enlace,creador_id)
);

CREATE TABLE sesion (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  array TEXT NOT NULL,
  titulo TEXT NOT NULL,
  descripcion TEXT NOT NULL,
  creador_id INTEGER NOT NULL,
  FOREIGN KEY (creador_id) REFERENCES usuario (id),
  UNIQUE(titulo,creador_id)
);