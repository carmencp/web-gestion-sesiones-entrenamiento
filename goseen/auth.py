import functools

from flask import (
    Blueprint, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from goseen.db import get_db

# blueprint para las funciones de autenticación

# el url_prefix se añadirá a todas las URLs asociadas al blueprint
bp = Blueprint('auth', __name__, url_prefix='/auth')


''' código para la vista de registro,
@bp.route asocia la URL /registro con la función de la vista de registro '''
@bp.route('/registro', methods=('GET', 'POST'))
def registro():

    error = None

    ''' si el usuario ha enviado el formulario, request.method será 'POST',
    request.form mapea las claves y valores del formulario enviado '''
    if request.method == 'POST':
        nombre = request.form['nombre']
        apellidos = request.form['apellidos']
        email = request.form['email']
        password = request.form['password']
        password2 = request.form['password2']

        # validación de que los campos obligatorios no estén vacíos
        if not nombre:
            error = 'Debes introducir un nombre.'
        elif not email:
            error = 'Debes introducir tu correo electrónico.'
        elif not password:
            error = 'Debes introducir una contraseña.'
        elif not password2:
            error = 'Debes repetir la contraseña.'
        elif password != password2:
            error = 'Las contraseñas deben coincidir.'

        # si la validación tiene éxito, inserta los nuevos datos en la base de datos
        if error is None:
            try:
                db = get_db()
                # se utiliza generate_password_hash() para hacer un hash seguro de la contraseña, y ese hash se almacena
                db.execute(
                    "INSERT INTO usuario (nombre, apellidos, email, password) VALUES (?, ?, ?, ?)",
                    (nombre, apellidos, email, generate_password_hash(password)),
                )
                # db.commit() para guardar los cambios
                db.commit()
                # se producirá un sqlite3.IntegrityError si el email ya existe, puesto que está definido como único en la bd
            except db.IntegrityError:
                error = f"Ya existe un usuario con ese correo."
            else:
                session.clear()
                # se le redirige a la página de inicio de sesión
                return redirect(url_for("auth.login"))

    ''' cuando el usuario navega inicialmente a auth/registro, o hay un error de validación, 
    se debe mostrar una página HTML con el formulario de registro, 
    render_template() renderizará la plantilla que contiene el HTML '''
    return render_template('auth/registro.html', error=error)


''' código para la vista de login,
@bp.route asocia la URL /login con la función de la vista de inicio de sesión '''
@bp.route('/login', methods=('GET', 'POST'))
def login():

    error = None
    
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        # el usuario se consulta primero y se almacena en una variable para su uso posterior
        db = get_db()
        usuario = db.execute(
            'SELECT * FROM usuario WHERE email = ?', (email,)
        ).fetchone()

        if usuario is None:
            error = 'Correo electrónico incorrecto.'
        # check_password_hash() realiza el hash de la contraseña enviada de la misma forma 
        # que el hash almacenado y los compara de forma segura. Si coinciden, la contraseña es válida.
        elif not check_password_hash(usuario['password'], password):
            error = 'Contraseña incorrecta.'

        if error is None:
            ''' session almacena datos a través de las peticiones, 
            cuando la validación tiene éxito, el id del usuario se almacena en una nueva sesión '''
            session.clear()
            session['id_usuario'] = usuario['id']
            return redirect(url_for('index'))

    return render_template('auth/login.html', error=error)


''' si un usuario está logueado, esta función cargará su información.
bp.before_app_request() registra una función que se ejecuta antes de 
la función de vista, sin importar la URL solicitada '''
@bp.before_app_request
def load_logged_in_user():
    id_usuario = session.get('id_usuario')

    if id_usuario is None:
        g.usuario = None
    else:
        g.usuario = get_db().execute(
            'SELECT * FROM usuario WHERE id = ?', (id_usuario,)
        ).fetchone()


# cerrar sesión
@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


# función para requerir que un usuario esté logueado
def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.usuario is None:
            # si no hay un usuario logueado, redirige al login
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view