import sqlite3

import click
from flask import current_app, g
from flask.cli import with_appcontext


# conexión a la base de datos
def get_db():
    # usamos g para almacenar datos que pueden ser accedidos por múltiples funciones durante una petición
    if 'db' not in g:
        # establece una conexión con el fichero apuntado por la clave de configuración DATABASE
        g.db = sqlite3.connect(
            # current_app apunta a la aplicación Flask que maneja la solicitud
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        ''' sqlite3.Row indica a la conexión que devuelva filas que se comporten como dicts, 
        esto permite acceder a las columnas por su nombre. '''
        g.db.row_factory = sqlite3.Row

    return g.db


''' para cerrar la conexión a la bd, comprueba si se ha creado una conexión 
comprobando si se ha establecido g.db. Si la conexión existe, se cierra '''
def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


# inicializa la bd
def init_db():
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))

# llama a init-db y muestra mensaje
@click.command('init-db')
@with_appcontext
def init_db_command():
    # Limpia los datos existentes y crea nuevas tablas
    init_db()
    click.echo('Base de datos inicializada.')


# registro de las funciones con la instancia de la aplicación
def init_app(app):
    # le dice a Flask que llame a esa función cuando se limpie después de devolver la respuesta
    app.teardown_appcontext(close_db)
    # añade el comando para que pueda ser llamado con el comando flask (flask init-db)
    app.cli.add_command(init_db_command)