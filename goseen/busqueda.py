from pyexpat.errors import messages
from flask import (
    Blueprint, g, render_template, request
)

from goseen.auth import login_required
from goseen.db import get_db

# blueprint para las funciones de búsqueda

# el url_prefix se añadirá a todas las URLs asociadas al blueprint
bp = Blueprint('busqueda', __name__, url_prefix='/busqueda')


@bp.route('/buscarSesion', methods=('GET', 'POST'))
@login_required
def buscarSesion():

    ''' si el usuario ha enviado el formulario, request.method será 'POST',
    request.form mapea las claves y valores del formulario enviado '''
    if request.method == 'POST':

        db = get_db()

        # recoge lo que se ha introducido en el campo de búsqueda
        titulo = request.form['busquedasesion']

        # y devuelve los resultados
        resultado = db.execute(
            'SELECT * FROM sesion'
            ' WHERE titulo = ? AND creador_id = ?',(titulo,str(g.usuario['id']))
        ).fetchall()

    return render_template('sesiones/resultado.html', resultado=resultado)