from flask import (
    Blueprint, g, redirect, render_template, request, url_for
)

from goseen.auth import login_required
from goseen.db import get_db

from urllib.parse import urlparse
from urllib.parse import parse_qs

# blueprint para las funciones relacionadas con los vídeos

# el url_prefix se añadirá a todas las URLs asociadas al blueprint
bp = Blueprint('videos', __name__, url_prefix='/videos')


''' código para mostrar los vídeos de una categoría,
@bp.route asocia la URL /mostrarVideos con la función de la vista para mostrar los vídeos '''
@bp.route('/mostrarVideos')
@login_required
def mostrarVideos():
    db = get_db()

    # recogemos el id de la categoría que hemos pasado por la URL
    requestUrl = request.url
    parsed_url = urlparse(requestUrl)
    captured_value = parse_qs(parsed_url.query)['id'][0]

    # obtenemos los vídeos de esa categoría
    videos = db.execute(
        'SELECT * FROM video WHERE categoria_id = ? AND creador_id = ?'
        ,(captured_value, str(g.usuario['id']))
    ).fetchall()

    cat = db.execute(
        'SELECT nombre, descripcion FROM categoria WHERE id = ? AND creador_id = ?'
        ,(captured_value, str(g.usuario['id']))
    ).fetchone()

    return render_template('videos/mostrarVideos.html', videos=videos, categoria=captured_value, cat=cat)


''' código para subir un vídeo a una categoría,
@bp.route asocia la URL /subirVideo con la función de la vista para subir un vídeo '''
@bp.route('/subirVideo', methods=('GET', 'POST'))
@login_required
def subirVideo():

    db = get_db()
    error = None

    categorias = db.execute(
        'SELECT * FROM categoria WHERE creador_id = ?'
        ' ORDER BY nombre',(str(g.usuario['id']))
    ).fetchall()
    db.commit()

    if request.method == 'POST':
        enlace = request.form['enlace']
        categoria_id = request.form['categoria_id']

        if not enlace:
            error = 'Debes introducir el enlace del vídeo.'
        elif not categoria_id:
            error = 'Debes seleccionar la categoría donde subir el vídeo.'

        if error is None:
            id_enlace = enlace[32:43]

            cate = db.execute(
                'SELECT id FROM categoria WHERE nombre = ? AND creador_id = ?',
                (categoria_id, str(g.usuario['id']))
            ).fetchone()
            db.commit()

            try:
                db.execute(
                    'INSERT INTO video (enlace, categoria_id, creador_id)'
                    ' VALUES (?, ?, ?)',
                    (id_enlace, cate["id"], g.usuario['id'])
                )
                db.commit()
            except db.IntegrityError:
                error = f"Ese vídeo ya se encuentra subido en la web."
            else:
                return redirect(url_for('categorias.index'))

    return render_template('videos/subirVideo.html', categorias=categorias, error=error)


''' código para visualizar un vídeo,
@bp.route asocia la URL /verVideo con la función de la vista para visualizar un vídeo '''
@bp.route('/verVideo')
@login_required
def verVideo():

    db = get_db()

    # recogemos el id del vídeo que hemos pasado por la URL
    requestUrl = request.url
    parsed_url = urlparse(requestUrl)
    captured_value = parse_qs(parsed_url.query)['id'][0]

    video = db.execute(
        'SELECT * FROM video WHERE id = ? AND creador_id = ?'
        ,(captured_value, str(g.usuario['id']))
    ).fetchone()

    return render_template('videos/verVideo.html', video=video)


''' código para eliminar un vídeo,
Esta función toma un argumento, id. Este corresponde al <int:id> de la ruta y al id del vídeo. 
Flask capturará el id, se asegurará de que es un int, y lo pasará como el argumento id '''
@bp.route('/<int:id>/eliminarVideo', methods=('POST',))
@login_required
def eliminarVideo(id):

    db = get_db()
    db.execute('DELETE FROM video WHERE id = ? AND creador_id = ?',(id, str(g.usuario['id'])))
    db.commit()
    return redirect(url_for('categorias.index'))