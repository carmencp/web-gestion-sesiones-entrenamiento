import os

from flask import Flask

# application factory function
def create_app(test_config=None):
    ''' crea la instancia Flask,
    __name__ es el nombre del módulo actual de Python,
    instance_relative_config=True indica a la aplicación que 
    los archivos de configuración son relativos a la carpeta instance '''
    app = Flask(__name__, instance_relative_config=True)
    ''' establece alguna configuración por defecto que la aplicación utilizará,
    SECRET_KEY es utilizada por Flask y las extensiones para mantener los datos seguros,
    DATABASE es la ruta donde se guardará el archivo de la base de datos SQLite '''
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'goseen.sqlite'),
    )

    if test_config is None:
        # anula la configuración por defecto con valores tomados del archivo config.py, si existe
        app.config.from_pyfile('config.py', silent=True)
    else:
        # se utilizaría en lugar de la configuración de la instancia para los tests si se le pasa
        app.config.from_mapping(test_config)

    # asegura que app.instance_path existe
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # importación y llamada de funciones

    from . import db
    db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import categorias
    app.register_blueprint(categorias.bp)
    app.add_url_rule('/', endpoint='index')

    from . import videos
    app.register_blueprint(videos.bp)
    app.add_url_rule('/', endpoint='videos')

    from . import sesiones
    app.register_blueprint(sesiones.bp)
    app.add_url_rule('/', endpoint='sesiones')

    from . import perfil
    app.register_blueprint(perfil.bp)
    app.add_url_rule('/', endpoint='perfil')

    from . import busqueda
    app.register_blueprint(busqueda.bp)
    app.add_url_rule('/', endpoint='busqueda')

    return app