from pyexpat.errors import messages
from flask import (
    Blueprint, g, redirect, render_template, request, url_for
)
from werkzeug.security import generate_password_hash

from goseen.auth import login_required
from goseen.db import get_db

# blueprint para las funciones relacionadas con el perfil de usuario

# el url_prefix se añadirá a todas las URLs asociadas al blueprint
bp = Blueprint('perfil', __name__, url_prefix='/perfil')


''' código para ver los datos del perfil,
@bp.route asocia la URL /verPerfil con la función de la vista para ver el perfil '''
@bp.route('/verPerfil')
@login_required
def verPerfil():
    db = get_db()

    datos = db.execute(
        'SELECT * FROM usuario'
        ' WHERE id = '+str(g.usuario['id'])
    ).fetchone()

    return render_template('perfil/verPerfil.html', datos=datos)


''' código para editar los datos del perfil,
@bp.route asocia la URL /editarPerfil con la función de la vista para editar el perfil '''
@bp.route('/editarPerfil', methods=('GET', 'POST'))
@login_required
def editarPerfil():

    db = get_db()
    error = None

    if request.method == 'POST':
        nombre = request.form['nombre']
        apellidos = request.form['apellidos']
        email = request.form['email']

        if not nombre:
            error = 'Debes introducir un nombre.'
        elif not email:
            error = 'Debes introducir un email.'

        if error is None:
            try:
                db = get_db()
                db.execute(
                    'UPDATE usuario SET nombre = ?, apellidos = ?, email = ?'
                    ' WHERE id = ?',
                    (nombre, apellidos, email, g.usuario['id'])
                )
                db.commit()
            except db.IntegrityError:
                error = f"Ya existe un usuario con ese correo."
            else:
                return redirect(url_for('perfil.verPerfil'))

    datos = db.execute(
        'SELECT * FROM usuario u'
        ' WHERE id = '+str(g.usuario['id'])
    ).fetchone()

    return render_template('perfil/editarPerfil.html', datos=datos, error=error)


''' código para cambiar la contraseña,
@bp.route asocia la URL /cambiarContrasena con la función de la vista para cambiar la contraseña '''
@bp.route('/cambiarContrasena', methods=('GET', 'POST'))
@login_required
def cambiarContrasena():
    db = get_db()

    error = None

    if request.method == 'POST':
        password = request.form['password']
        password2 = request.form['password2']

        if not password:
            error = 'Debes introducir una contraseña.'
        elif not password2:
            error = 'Debes repetir la contraseña.'
        elif password != password2:
            error = 'Las contraseñas deben coincidir.'

        if error is None:
            db = get_db()
            db.execute(
                'UPDATE usuario SET password = ?'
                ' WHERE id = ?',
                # se utiliza generate_password_hash() para hacer un hash seguro de la contraseña, y ese hash se almacena
                (generate_password_hash(password), g.usuario['id'])
            )
            db.commit()
            return redirect(url_for('perfil.verPerfil'))

    datos = db.execute(
        'SELECT * FROM usuario u'
        ' WHERE id = '+str(g.usuario['id'])
    ).fetchone()

    return render_template('perfil/cambiarContrasena.html', datos=datos, error=error)